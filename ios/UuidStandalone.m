#import "UuidStandalone.h"

@implementation UuidStandalone

RCT_EXPORT_MODULE()

RCT_REMAP_METHOD(randomUUID, randomUUID:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
	NSUUID *uuid = [NSUUID UUID];
	NSString *str = [uuid UUIDString];
	resolve(str);
}

@end

package com.reactnativeuuidstandalone;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;
import java.util.UUID;

@ReactModule(name = UuidStandaloneModule.NAME)
public class UuidStandaloneModule extends ReactContextBaseJavaModule {
    public static final String NAME = "UuidStandalone";

    public UuidStandaloneModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }


    // Example method
    // See https://reactnative.dev/docs/native-modules-android
    @ReactMethod
    public void randomUUID(Promise promise) {
        promise.resolve(UUID.randomUUID().toString());
    }
}

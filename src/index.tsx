import { NativeModules } from 'react-native';

type UuidStandaloneType = {
  randomUUID(): Promise<string>;
};

const UuidStandalone = NativeModules.UuidStandalone as UuidStandaloneType;

export const randomUUID = async () => {
  return (await UuidStandalone.randomUUID()).toLowerCase();
};

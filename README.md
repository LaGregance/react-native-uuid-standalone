# react-native-uuid-standalone

A standalone package that generate UUID based on native code.
Others UUID package compatible with React Native suffer from problems with buffer
or crypto dependencies. This one rely directly on UUID from Java for Android
and UUID from Objective-C for iOS.

## Installation

```sh
npm install react-native-uuid-standalone
```

## Usage

```js
import { randomUUID } from "react-native-uuid-standalone";
const uuid = await randomUUD(); // Return lowercase string UUID
```
